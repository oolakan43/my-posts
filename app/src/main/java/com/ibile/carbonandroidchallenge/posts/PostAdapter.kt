package com.ibile.carbonandroidchallenge.posts

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ibile.carbonandroidchallenge.databinding.AdapterPostsBinding
import com.ibile.mydata.data.post.Post

class PostAdapter(private var listOfPosts: List<Post>, private val postClickListener: OnPostClickListener) :
    RecyclerView.Adapter<PostViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val binding = AdapterPostsBinding.inflate(LayoutInflater.from(parent.context) ,parent, false)

        return PostViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return listOfPosts.size
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        val post = listOfPosts[position]
        holder.bindView(post)
        holder.itemView.setOnClickListener {
            postClickListener.postClicked(post)
        }
    }

    fun updatePosts(posts: List<Post>) {
        listOfPosts = posts
        notifyDataSetChanged()
    }
}

class PostViewHolder(val binding: AdapterPostsBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bindView(post: Post) {
        itemView.apply {

            binding.postTitle.text = post.title
            binding.postBody.text = post.body
        }
    }
}

interface OnPostClickListener {
    fun postClicked(post: Post)
}
