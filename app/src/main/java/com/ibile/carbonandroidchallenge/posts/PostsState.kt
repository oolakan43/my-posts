package com.ibile.carbonandroidchallenge.posts

import com.ibile.mydata.data.post.Post


sealed class PostsState {
    object Loading : PostsState()
    data class Error(val message: String?) : PostsState()
    data class PostsLoaded(val posts: List<Post>) : PostsState()
}