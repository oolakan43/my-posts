package com.ibile.carbonandroidchallenge.posts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ibile.appdata.data.CouroutineViewModel
import com.ibile.carbonandroidchallenge.data.Repository
import com.ibile.mydata.data.sharedpreference.SharedPreference
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

class PostsViewModel(private val repository: Repository, uiContext: CoroutineContext = Dispatchers.Main) :
    CouroutineViewModel(uiContext) {

    private val privateState = MutableLiveData<PostsState>()

    val postLiveData: LiveData<PostsState> = privateState

    fun refreshPosts() = launch {
        privateState.value = PostsState.Loading
        Timber.d(privateState.value.toString())

        privateState.value = repository.getPostsState()
        Timber.d(privateState.value.toString())
    }

}