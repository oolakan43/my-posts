    package com.ibile.carbonandroidchallenge

import com.ibile.appdata.data.NetworkState
import com.ibile.carbonandroidchallenge.comments.CommentsViewModel
import com.ibile.carbonandroidchallenge.data.Repository
import com.ibile.carbonandroidchallenge.data.RepositoryImpl
import com.ibile.carbonandroidchallenge.posts.PostsViewModel
import com.ibile.mydata.data.Api
import com.ibile.mydata.utils.Constant
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val dataSourceModule = module {

    single { createRetrofitInstance() }

    single { get<Retrofit>().create(Api::class.java) }

    single { NetworkState(androidApplication()) }

    single { RepositoryImpl(get(), get(), androidApplication()) as Repository }

}

val viewmodelModule = module {
    viewModel { PostsViewModel(get()) }

    viewModel { CommentsViewModel(get()) }
}

fun createRetrofitInstance(): Retrofit {
    return Retrofit.Builder()
        .baseUrl(Constant.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}