package com.ibile.carbonandroidchallenge.comments

import com.ibile.mydata.data.comment.Comment


sealed class CommentsState {
    object Loading : CommentsState()
    data class Error(val message: String?) : CommentsState()
    data class PostsLoaded(val comments: List<Comment>) : CommentsState()
}