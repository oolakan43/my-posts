package com.ibile.carbonandroidchallenge.comments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.ibile.appdata.data.MainThreadScope
import com.ibile.carbonandroidchallenge.R
import com.ibile.carbonandroidchallenge.data.CommentsAdapter
import com.ibile.carbonandroidchallenge.databinding.FragmentCommentsBinding
import com.ibile.mydata.data.comment.CommentList
import com.ibile.mydata.data.sharedpreference.SharedPreference
import org.koin.androidx.viewmodel.ext.android.getViewModel
import timber.log.Timber

class CommentsFragment : Fragment() {
    private lateinit var commentsViewModel: CommentsViewModel
    private val uiScope = MainThreadScope()
    private var _binding: FragmentCommentsBinding? = null
    private val binding get() = _binding!!

    private val args: CommentsFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(uiScope)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentCommentsBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as AppCompatActivity).supportActionBar?.apply {
            show()
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
            title = getString(R.string.comments)
        }

        val commentsAdapter = CommentsAdapter(emptyList())

        binding.commentsRecyclerView?.apply {
            layoutManager = LinearLayoutManager(activity)
            setHasFixedSize(true)
            adapter = commentsAdapter
        }

        commentsViewModel = getViewModel()
        commentsViewModel.commentsLiveData.observe(viewLifecycleOwner, Observer { commentState ->
            if (commentState == null) {
                return@Observer
            }

            when (commentState) {
                is CommentsState.Loading -> {
                    setUpdateLayoutVisibilty(View.VISIBLE)
                }

                is CommentsState.Error -> {
                    setUpdateLayoutVisibilty(View.GONE)
                    context?.let {

                        val message = commentState.message ?: getString(R.string.error)
                        val comments = SharedPreference.getCommentLists(requireContext(), args.postId.toString());

                        if (comments.commentList.isNullOrEmpty()) {
                            Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
                        } else {
                            setUpdateLayoutVisibilty(View.GONE)
                            commentsAdapter.updateComments(comments.commentList)
                        }
                    }
                }

                is CommentsState.PostsLoaded -> {
                    setUpdateLayoutVisibilty(View.GONE)
                    var commentList = CommentList(commentState.comments)
                    SharedPreference.setPreferenceObject(requireContext(), commentList, commentState.comments.get(0).id.toString())
                    commentsAdapter.updateComments(commentState.comments)
                }
            }
        })
        commentsViewModel.getComments(args.postId)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                requireActivity().onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    fun setUpdateLayoutVisibilty(value: Int) {
        binding.updateLayout.apply {
            visibility = value
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
