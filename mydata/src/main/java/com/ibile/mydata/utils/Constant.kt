package com.ibile.mydata.utils

object Constant {
    const val BASE_URL = "https://jsonplaceholder.typicode.com"
    const val POST = "post"
    const val POSTS = "posts"
    const val COMMENTS = "comments"
    const val POST_ID = "postId"

}