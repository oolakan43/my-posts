package com.ibile.mydata.data.sharedpreference

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import com.ibile.mydata.data.comment.CommentList
import com.ibile.mydata.data.post.PostList


object SharedPreference {
    fun setPreferenceObject(c: Context, `object`: Any?, key: String?) {
        /**** storing object in preferences   */
        try {
            val appSharedPrefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(
                c.applicationContext
            )
            val prefsEditor = appSharedPrefs.edit()
            val gson = Gson()
            val jsonObject = gson.toJson(`object`)
            prefsEditor.putString(key, jsonObject)
            prefsEditor.apply()
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }
    }

    fun getPostLists(c: Context, key: String?): PostList {
        val appSharedPrefs: SharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(
                c.applicationContext
            )
        /**** get agent data     */
        val json = appSharedPrefs.getString(key, "")
        val gson = Gson()
        val postResponse: PostList
        if (json.isNullOrEmpty())
            postResponse = PostList(emptyList())
        else
            postResponse = gson.fromJson(json, PostList::class.java)

        return postResponse
    }


    fun getCommentLists(c: Context, key: String): CommentList {
        val appSharedPrefs: SharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(
                c.applicationContext
            )
        /**** get agent data     */
        val json = appSharedPrefs.getString(key, "")
        val gson = Gson()
        val commentResponse: CommentList
        if (json.isNullOrEmpty())
            commentResponse = CommentList(emptyList())
        else
            commentResponse = gson.fromJson(json, CommentList::class.java)
        return commentResponse
    }

}