package com.ibile.mydata.data.comment

data class CommentList(val commentList: List<Comment>)
