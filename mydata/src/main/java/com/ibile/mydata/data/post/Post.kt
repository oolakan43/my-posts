package com.ibile.mydata.data.post

data class Post(val userId: Int, val id: Int, val title: String, val body: String)
