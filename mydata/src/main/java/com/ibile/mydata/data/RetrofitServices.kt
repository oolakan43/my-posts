package com.ibile.mydata.data
import com.ibile.mydata.data.comment.Comment
import com.ibile.mydata.data.post.Post
import com.ibile.mydata.utils.Constant
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {
    @GET(Constant.POSTS)
    suspend fun getPostsResponse(): Response<List<Post>>

    @GET(Constant.COMMENTS)
    suspend fun getComments(@Query(Constant.POST_ID) postId: Int): Response<List<Comment>>
}

